package com.mts.teta.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class User {
    private String phone;
    private String firstName;
    private String lastName;
}
