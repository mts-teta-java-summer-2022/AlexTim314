package com.mts.teta.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Message {
    private String content;
    private EnrichmentType enrichmentType;

    public enum EnrichmentType {
        MSISDN
    }
}
