package com.mts.teta.data;

import com.mts.teta.model.User;

import java.util.concurrent.ConcurrentHashMap;

public class UserStorage {

    private final ConcurrentHashMap<String, User> users = new ConcurrentHashMap<>();

    public User put(String phone, User user){
        return users.put(phone, user);
    }

    public User get(String phone){
        return users.get(phone);
    }

}
