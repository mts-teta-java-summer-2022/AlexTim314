package com.mts.teta.data;

import com.mts.teta.model.Message;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

public class MessageStorage {

    private final BlockingQueue<Message> enriched = new LinkedBlockingQueue<>();

    private final BlockingQueue<Message> unenriched = new LinkedTransferQueue<>();

    public void putEnriched(Message message) throws InterruptedException {
        enriched.put(message);
    }

    public void putUnenriched(Message message) throws InterruptedException {
        unenriched.put(message);
    }

    public Message poolEnriched(int timeOutSec) throws InterruptedException {
        return enriched.poll(timeOutSec, TimeUnit.SECONDS);
    }

    public Message poolUnenriched(int timeOutSec) throws InterruptedException {
        return unenriched.poll(timeOutSec, TimeUnit.SECONDS);
    }

    public int enrichedSize(){
        return enriched.size();
    }

    public int unenrichedSize(){
        return unenriched.size();
    }

}
