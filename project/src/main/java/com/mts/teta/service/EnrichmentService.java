package com.mts.teta.service;

import com.mts.teta.data.MessageStorage;
import com.mts.teta.data.UserStorage;
import com.mts.teta.enricher.EnrichingResult;
import com.mts.teta.enricher.MessageEnricher;
import com.mts.teta.enricher.MsisdnEnricher;
import com.mts.teta.model.Message;
import com.mts.teta.model.User;
import lombok.AllArgsConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONParser;

@AllArgsConstructor
public class EnrichmentService {

    private UserStorage userStorage;
    private MessageStorage messageStorage;

    // возвращается обогащенный (или необогащенный) content сообщения
    public String enrich(Message message) throws InterruptedException {
        String content = message.getContent();
        MessageEnricher messageEnricher;
        switch (message.getEnrichmentType()) {
            case MSISDN:
                messageEnricher = new MsisdnEnricher();
                break;
            default:
                messageStorage.putUnenriched(message);
                return content;
        }
        String phone;
        try {
            JSONObject jsonMessage = new JSONObject(content);
            phone = jsonMessage.getString("msisdn");
        } catch (JSONException e) {
            messageStorage.putUnenriched(message);
            return content;
        }
        User user = userStorage.get(phone);
        EnrichingResult enrichingResult = messageEnricher.enrich(message.getContent(), user);
        if (enrichingResult.isSuccess()) {
            message.setContent(enrichingResult.getContent());
            messageStorage.putEnriched(message);
        } else {
            messageStorage.putUnenriched(message);
        }
        return enrichingResult.getContent();
    }
}
