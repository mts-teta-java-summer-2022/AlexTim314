package com.mts.teta.enricher;

import com.mts.teta.model.User;

public interface MessageEnricher {

    EnrichingResult enrich(String message, User user);

}
