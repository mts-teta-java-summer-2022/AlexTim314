package com.mts.teta.enricher;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class EnrichingResult {
    private boolean success;
    private String content;
}
