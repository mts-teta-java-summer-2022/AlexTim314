package com.mts.teta.enricher;

import com.mts.teta.model.User;
import org.json.JSONException;
import org.json.JSONObject;

public class MsisdnEnricher implements MessageEnricher {
    @Override
    public EnrichingResult enrich(String message, User user) {
        if (user != null) {
            try {
                JSONObject jsonMessage = new JSONObject(message);
                JSONObject enrichment = new JSONObject();
                enrichment.put("firstName", user.getFirstName());
                enrichment.put("lastName", user.getLastName());
                jsonMessage.put("enrichment", enrichment);
                return EnrichingResult.builder()
                        .success(true)
                        .content(jsonMessage.toString())
                        .build();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return EnrichingResult.builder()
                .success(false)
                .content(message)
                .build();
    }
}
