package com.mts.teta.enricher;

import com.mts.teta.model.User;
import org.json.JSONException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class MsisdnEnricherTest {
    private static Stream<Arguments> provideShouldEnrichSuccessfully() {
        return Stream.of(
                arguments("{\"action\": \"button_click\", \"page\": \"book_card\", \"msisdn\": \"88005553535\"},",
                        "{\"action\": \"button_click\", \"page\": \"book_card\", \"msisdn\": \"88005553535\", \"enrichment\": {\"firstName\": \"Vasy\", \"lastName\": \"Ivanv\"}}",
                        "Vasy", "Ivanv"
                ),
                arguments("{\"action\": \"button_click\", \"page\": \"book_card\", \"msisdn\": \"88005553535\", \"enrichment\": {\"firstName\": \"NO_Vasy\", \"lastName\": \"other_name\"}},",
                        "{\"action\": \"button_click\", \"page\": \"book_card\", \"msisdn\": \"88005553535\", \"enrichment\": {\"firstName\": \"Vasy\", \"lastName\": \"Ivanv\"}}",
                        "Vasy", "Ivanv"
                ),
                arguments("{\"msisdn\": \"8\"}",
                        "{\"msisdn\": \"8\", \"enrichment\": {\"firstName\": \"V\", \"lastName\": \"I\"}}"
                        , "V", "I"
                )
        );
    }

    private static Stream<Arguments> provideShouldEnrichFail() {
        return Stream.of(
                arguments("Not JSON"),
                arguments("{\"without \": \"msisdn\"}",
                        "{\"action\": \"button_click\", \"page\": \"book_card\", \"msisdn\": \"88005553535\", \"enrichment\": {\"firstName\": \"Vasy\", \"lastName\": \"Ivanv\"}}",
                        "Vasy", "Ivanv"
                ),
                arguments("{\"msisdn\": \"8\"}",
                        "{\"msisdn\": \"8\", \"enrichment\": {\"firstName\": \"V\", \"lastName\": \"I\"}}"
                        , "V", "I"
                )
        );
    }

    /**
     * Тест на корректное обогащение валидного сообщения.
     *
     * @param messageContent пользователь
     */
    @ParameterizedTest
    @MethodSource("provideShouldEnrichSuccessfully")
    void shouldEnrichValidContent(String messageContent, String expected, String firstName, String lastName) throws JSONException {
        //given
        MsisdnEnricher enricher = new MsisdnEnricher();
        User user = User.builder().firstName(firstName).lastName(lastName).build();
        //when
        EnrichingResult actual = enricher.enrich(messageContent, user);
        //then
        assertTrue(actual.isSuccess());
        JSONAssert.assertEquals(expected, actual.getContent(), true);
    }

    /**
     * Тест на возврат исходного сообщения, если не удовлетворяет условиям обагащения.
     *
     * @param messageContent пользователь
     */
    @ParameterizedTest
    @ValueSource(strings = {
            "Not JSON",
            "{\"invalid JSON \": }",
            "Another not JSON",
            ""
    })
    void shouldNoEnrichInvalidContent(String messageContent){
        //given
        MsisdnEnricher enricher = new MsisdnEnricher();
        User user = User.builder().firstName("firstName").lastName("lastName").build();
        //when
        EnrichingResult actual = enricher.enrich(messageContent, user);
        //then
        assertFalse(actual.isSuccess());
        assertEquals(messageContent, messageContent);
    }
}