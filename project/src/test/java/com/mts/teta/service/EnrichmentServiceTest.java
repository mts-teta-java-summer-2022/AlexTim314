package com.mts.teta.service;

import com.mts.teta.data.MessageStorage;
import com.mts.teta.data.UserStorage;
import com.mts.teta.model.Message;
import com.mts.teta.model.User;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EnrichmentServiceTest {

    @Test
    void enrich() throws InterruptedException {
        //given
        UserStorage userStorage = new UserStorage();
        String phone1 = "89123123123";
        String phone2 = "88005553535";
        String phone3 = "0";
        User user1 = User.builder().phone(phone1).firstName("firstName").lastName("lastName").build();
        User user2 = User.builder().phone(phone2).firstName("Vasya").lastName("Ivanov").build();
        User user3 = User.builder().phone(phone3).firstName("M").lastName("T").build();
        userStorage.put(phone1, user1);
        userStorage.put(phone2, user2);
        userStorage.put(phone3, user3);
        Message validMessage1 = Message.builder().enrichmentType(Message.EnrichmentType.MSISDN)
                .content(String.format("{\"msisdn\": \"%s\"}", user1.getPhone())).build();
        Message validMessage2 = Message.builder().enrichmentType(Message.EnrichmentType.MSISDN)
                .content(String.format("{\"msisdn\": \"%s\"}", user2.getPhone())).build();
        Message validMessage3 = Message.builder().enrichmentType(Message.EnrichmentType.MSISDN)
                .content(String.format("{\"msisdn\": \"%s\"}", user3.getPhone())).build();
        Message invalidMessage = Message.builder().enrichmentType(Message.EnrichmentType.MSISDN)
                .content("invalidMessage").build();
        MessageStorage messageStorage = new MessageStorage();
        EnrichmentService service = new EnrichmentService(userStorage, messageStorage);

        ExecutorService threadPool = Executors.newFixedThreadPool(5);
        CountDownLatch latch1 = new CountDownLatch(1);
        CountDownLatch latch2 = new CountDownLatch(1);
        CountDownLatch latch3 = new CountDownLatch(1);
        CountDownLatch latch4 = new CountDownLatch(1);

        //when
        threadPool.submit(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    service.enrich(validMessage1);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch1.countDown();
        });

        threadPool.submit(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    service.enrich(validMessage2);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch2.countDown();
        });

        threadPool.submit(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    service.enrich(validMessage3);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch3.countDown();
        });

        threadPool.submit(() -> {
            try {
                for (int i = 0; i < 10; i++) {
                    service.enrich(invalidMessage);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch4.countDown();
        });

        latch1.await();
        latch2.await();
        latch3.await();
        latch4.await();
        //then
        assertEquals(30, messageStorage.enrichedSize());
        assertEquals(10, messageStorage.unenrichedSize());
    }

}